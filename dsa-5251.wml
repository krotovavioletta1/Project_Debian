#use wml::debian::translation-check translation="6756ceee05381fa63bc4c02e4e26bd4902cb1ac4"
<define-tag description>Обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В DHCP-клиенте,
ретрансляторе и сервере ISC было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2928">CVE-2022-2928</a>

    <p>Было обнаружено, что DHCP-сервер некорректно выполняет
    подсчет ссылок на опции при настройке с параметром "allow  leasequery;".
    Удаленный злоумышленник может воспользоваться этим недостатком, чтобы вызвать отказ
    в обслуживании (сбой daemon).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2929">CVE-2022-2929</a>

    <p>Было обнаружено, что DHCP-сервер подвержен утечке памяти
    при обработке содержимого данных опции 81 (полное доменное имя), полученных в
    DHCP-пакете. Удаленный злоумышленник может воспользоваться этим недостатком,
    чтобы заставить DHCP-серверы потреблять ресурсы, что приведет к отказу
    в обслуживании.</p></li>

</ul>

<p>В стабильном дистрибутиве (bullseye), эти проблемы были исправлены в
версии 4.4.1-2.3+deb11u1.</p>

<p>Рекомендуется обновить пакеты isc-dhcp.</p>

<p>Для получения подробной информации о статусе безопасности isc-dhcp обратитесь к его
странице отслеживания безопасности по ссылке:
<a href="https://security-tracker.debian.org/tracker/isc-dhcp">https://security-tracker.debian.org/tracker/isc-dhcp</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5251.data"
